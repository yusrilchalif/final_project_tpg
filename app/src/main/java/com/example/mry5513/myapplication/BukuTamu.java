package com.example.mry5513.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class BukuTamu extends android.app.Fragment {
    public BukuTamu(){}
    RelativeLayout view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = (RelativeLayout) inflater.inflate(R.layout.buku_tamu, container, false);

        getActivity().setTitle("Buku Tamu");

        return view;
    }
}
