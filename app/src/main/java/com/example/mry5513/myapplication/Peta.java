package com.example.mry5513.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Peta extends android.app.Fragment implements OnMapReadyCallback {
    public Peta(){}
    RelativeLayout view;
    private GoogleMap googleMap;
    MapFragment mapFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = (RelativeLayout) inflater.inflate(R.layout.peta, container, false);

        getActivity().setTitle("Peta");

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
